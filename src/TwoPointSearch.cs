﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AlgorithmTest
{
    /// <summary>
    /// 二分法查找
    /// </summary>
    class TwoPointSearch
    {
        static int DeepNum = 0;
        static int SearchNum = 500;

        static void Main(string[] args)
        {
            var arrayList = new List<int>();
            for (int i = 0; i < 1000; i++)
            {
                arrayList.Add(i);
            }

            int returnIndex = TwoPointsSearch(arrayList.ToArray(), SearchNum, 0, arrayList.Count - 1);
            Console.Write("Index of array element {0} is {1},DeepNum is {2}", SearchNum, returnIndex, DeepNum);
            Console.ReadKey();
        }

        /// <summary>
        /// returns index, returns -1 if it does not exist
        /// </summary>
        /// <param name="array"></param>
        /// <param name="data"></param>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static int TwoPointsSearch(int[] array, int data, int leftIndex, int rightIndex)
        {
            DeepNum++;
            int MiddleIndex = (leftIndex + rightIndex) / 2;
            if (leftIndex > rightIndex)
            {
                return -1;
            }
            else
            {
                if (array[MiddleIndex] == data)
                {
                    return MiddleIndex;
                }
                else
                {
                    if (array[MiddleIndex] > data)
                    {
                        return TwoPointsSearch(array, data, leftIndex, MiddleIndex - 1);
                    }
                    else
                    {
                        return TwoPointsSearch(array, data, MiddleIndex + 1, rightIndex);
                    }
                }
            }
        }
    }
}
